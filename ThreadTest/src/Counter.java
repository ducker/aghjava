
public class Counter {
	private long count = 0;
	
	public synchronized void increment() {
		count++;
	}
	
	public synchronized void decrement() {
		count--;
	}
	
	public long get() {
		return count;
	}
}
