import java.util.concurrent.Callable;


public class Incrementer implements Callable<Void> {

	private Counter counter;
	private long repeat;
	
	public Incrementer(Counter counter, long repeat) {
		this.counter = counter;
		this.repeat = repeat;
	}
	
	public Void call() throws Exception {
		for (int i=0; i<repeat; i++) {
			counter.increment();
		}
		return null;
	}
}
