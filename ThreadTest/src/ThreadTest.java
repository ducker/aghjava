import java.util.ArrayList;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicLong;


public class ThreadTest {

	public static void main(String[] args) throws InterruptedException, ExecutionException {
		ExecutorService service = Executors.newFixedThreadPool(16);
		Counter counter = new Counter();
		Decrementer decrementer = new Decrementer(counter, 10000000);
		Incrementer incrementer = new Incrementer(counter, 10000000);
		ArrayList<Callable<Void>> taskArray = new ArrayList<Callable<Void>>();
		taskArray.add(incrementer);
		taskArray.add(decrementer);
		service.invokeAll(taskArray);
		System.out.println("Count " + counter.get());
		
		ConcurrentHashMap<Integer, AtomicLong> map = new ConcurrentHashMap<Integer, AtomicLong>();
		ExecutorService service2 = Executors.newFixedThreadPool(16);
		ArrayList<Callable<Void>> taskArray2 = new ArrayList<Callable<Void>>();
		for (int i=0; i<4; i++) {
			taskArray2.add(new Randomizer(map, 1000));
		}
		service2.invokeAll(taskArray2);
		long sum = 0;
		for (Integer key : map.keySet()) {
			System.out.println("Key: " + key + " Count: " + map.get(key));
			sum += map.get(key).get();
		}
		System.out.println("Sum: " + sum);
	}

}
