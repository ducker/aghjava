import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import com.thoughtworks.xstream.XStream;


public class SimpleSerialization {

	public static void main(String[] args) {
		SimpleObject object = new SimpleObject();
			try (FileOutputStream fos = new FileOutputStream("output.ser")) {
		    	try(ObjectOutputStream oos = new ObjectOutputStream(fos)) {   
		    		oos.writeObject(object);
		    	}
			} catch (IOException e) {
				
			}
			
			try (FileInputStream fis = new FileInputStream("output.ser")) { 
				try (ObjectInputStream ois = new ObjectInputStream(fis)) {
					SimpleObject readObject = (SimpleObject) ois.readObject();
					System.out.println("deserialized object property: " + readObject.someProperty);
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				}
			} catch (IOException e) {
				
			}
		    
			// XStream
			XStream xstream = new XStream();
			String xml = xstream.toXML(object);
			System.out.println("serialized:\n" + xml);
		    
			try (FileOutputStream fos = new FileOutputStream("outputXStream.xml");
					) {
				try(ObjectOutputStream oos = new ObjectOutputStream(fos)) {   
					oos.writeUTF(xml);
		    	}
			} catch (IOException e) {
				
			}
		    
			SimpleObject readStreamObject;
			try {
				readStreamObject = (SimpleObject) xstream.fromXML(new FileInputStream("outputXStream.xml"));
				System.out.println("deserialized object value: " + readStreamObject.someProperty);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

	}

}
