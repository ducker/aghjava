import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;


public class CopyFile {
	
	public static void main(String[] args) {
		System.out.println("file path to copy : ");
		try (BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in))){	
			String s = bufferRead.readLine();
			try (FileInputStream fileInputStream = new FileInputStream(s)){
				System.out.println("file destipation path : ");
				s = bufferRead.readLine();
				try (FileOutputStream fileOutputStream = new FileOutputStream(s)) {
					int c;
					while ((c = fileInputStream.read()) != -1) {
						fileOutputStream.write(c);
					}
				}
			}
		} catch (IOException e) {
			
		}
	}
}
