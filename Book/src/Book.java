import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Scanner;
import java.util.TreeMap;


public class Book {

	public static void main(String[] args) throws IOException {
		HashMap<String, Integer> hashMap = new HashMap<String, Integer>();
		TreeMap<String, Integer> treeMap = new TreeMap<String, Integer>();
		File textFile = new File("src/sampleBook.txt"); // or file path variable
		ArrayList<String> stringList = new ArrayList<String>();
		try {
			Scanner fileScanner = new Scanner(textFile);
			while (fileScanner.hasNext()) {
				String word = fileScanner.next();
				word = word.replaceAll("\\s","");
				stringList.add(word);
				hashMap.put(word, hashMap.get(word) == null ? 1 : hashMap.get(word) + 1);
				treeMap.put(word, treeMap.get(word) == null ? 1 : treeMap.get(word) + 1);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		Integer findWordLimit = 100;
		Random generator = new Random();
		while (stringList.size() > findWordLimit)
			stringList.remove(generator.nextInt(stringList.size()));
			
		long startTime = System.nanoTime();
		for (int i = 0; i < stringList.size(); i++)
			hashMap.get(stringList.get(i));
		System.out.println("HashMap: " + (System.nanoTime() - startTime));
		
		startTime = System.nanoTime();
		for (int i = 0; i < stringList.size(); i++)
			treeMap.get(stringList.get(i));
		System.out.println("TreeMap: " + (System.nanoTime() - startTime));
		
		Integer saveWordLimit = 50;
		ArrayList<Map.Entry<String, Integer>> entryList = new ArrayList<Map.Entry<String, Integer>>(hashMap.entrySet());
		Collections.sort(entryList, new MapEntryComparator());
		List<Map.Entry<String, Integer>> saveList = entryList.subList(entryList.size() - saveWordLimit, entryList.size());
		Collections.reverse(saveList);
		FileOutputStream fos = null;
		ObjectOutputStream oos = null;
		try {
		    fos = new FileOutputStream("output.txt");
		    oos = new ObjectOutputStream(fos);   
		    for (Entry<String, Integer> entry : saveList)
		    	oos.writeUTF(entry.getKey() + "\t" + entry.getValue() + "\n");
		    oos.close(); 
		} catch(Exception ex) {
		    ex.printStackTrace();
		} finally {
			fos.close();
			oos.close();
		}
		System.out.println("Finished");
	}
}
