import java.util.Comparator;
import java.util.Map;
import java.util.Map.Entry;


public class MapEntryComparator implements Comparator<Map.Entry<String, Integer>> {

	@Override
	public int compare(Entry<String, Integer> entry0, Entry<String, Integer> entry1) {
		return entry0.getValue().compareTo(entry1.getValue());
	}
	 

}
