import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Random;


public class List {
		
	public static void main(String[] args) {
		final Integer elementsCount = (int) Math.pow(10, 7);

		ArrayList<Integer> integerList = new ArrayList<Integer>();
		ArrayList<Integer> integerListWithCapacity = new ArrayList<Integer>(elementsCount);
		LinkedList<Integer> linkedList = new LinkedList<Integer>();

		Random generator = new Random();
		
		long startTime = System.currentTimeMillis();
		for (int i = 0; i < elementsCount; i++) {
			integerList.add(generator.nextInt());
		}
		System.out.println("integerList: " + (System.currentTimeMillis() - startTime) + "ms");
		startTime = System.currentTimeMillis();
		for (int i = 0; i < elementsCount; i++) {
			integerListWithCapacity.add(generator.nextInt());
		}
		System.out.println("integerListWithCapacity: " + (System.currentTimeMillis() - startTime) + "ms");
		for (int i = 0; i < elementsCount; i++) {
			linkedList.add(generator.nextInt());
		}
		System.out.println("linkedList: " + (System.currentTimeMillis() - startTime) + "ms");
		startTime = System.currentTimeMillis();
		
		int removeCount = 10000;
		startTime = System.currentTimeMillis();
		for (int i = 0; i < removeCount; i++) {
			integerList.remove(100);
		}
		System.out.println("remove integerList: " + (System.currentTimeMillis() - startTime) + "ms");
		startTime = System.currentTimeMillis();
		for (int i = 0; i < removeCount; i++) {
			integerListWithCapacity.remove(100);
		}
		System.out.println("remove integerListWithCapacity: " + (System.currentTimeMillis() - startTime) + "ms");
		startTime = System.currentTimeMillis();
		for (int i = 0; i < removeCount; i++) {
			linkedList.remove(100);
		}
		System.out.println("remove linkedList: " + (System.currentTimeMillis() - startTime) + "ms");
	}

}
