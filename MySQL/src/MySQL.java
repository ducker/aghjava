import java.io.File;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;
import java.util.Scanner;


public class MySQL {

	public static void main(String[] args) throws ClassNotFoundException, FileNotFoundException {
		// TODO Auto-generated method stub
		Class.forName("com.mysql.jdbc.Driver");
		Properties connectionProps = new Properties();
	    connectionProps.put("user", "root");
	    connectionProps.put("password", "Rabbit123");
	    try (Connection conn = DriverManager.getConnection(
				"jdbc:mysql://localhost:3306/AGH",
	               connectionProps);) {
		    
			Statement preparedStatement = conn.createStatement();
			File textFile = new File("src/bookmarks.csv");
			Calendar calendar = Calendar.getInstance();
		    java.sql.Date currentDate = new java.sql.Date(calendar.getTime().getTime());
			try (Scanner fileScanner = new Scanner(textFile);) {
				while (fileScanner.hasNext()) {
					String line = fileScanner.nextLine();
					String [] array = line.split(",");
					String query = "insert into Bookmarks (URL, category, description, date)"
						        + " values (?, ?, ?, ?)";
					PreparedStatement preparedStmt = conn.prepareStatement(query);
					preparedStmt.setString(1, array[0]);
					preparedStmt.setString(2, array[1]);
					preparedStmt.setString(3, array[2]);
					preparedStmt.setDate(4, currentDate);
					preparedStmt.execute();
				}
			}
			
			ResultSet resultSet = preparedStatement.executeQuery("select * from Bookmarks");
			while (resultSet.next()) {
				String url = resultSet.getString("URL");
			    String category = resultSet.getString("category");
			    String description = resultSet.getString("description");
			    Date date = resultSet.getDate("date");
			    System.out.println("URL: " + url);
			    System.out.println("Category: " + category);
			    System.out.println("Description: " + description);
			    System.out.println("Date: " + date);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
