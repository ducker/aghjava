import java.util.Random;
import java.util.concurrent.Callable;


public class PiCallable implements Callable<Double> {
	public Integer n = 1000000;
	@Override
	public Double call() throws Exception {
		Random random = new Random();
		Integer nCircle = 0;
		for (Integer i=0; i<n; i++){
			Double x = random.nextDouble() * 2 - 1;
			Double y = random.nextDouble() * 2 - 1;
			if (x*x + y*y <= 1) {
				nCircle++;
			}
		}
		return 4*nCircle.doubleValue()/n.doubleValue();
	}
}
