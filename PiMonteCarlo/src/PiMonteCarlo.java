import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;


public class PiMonteCarlo {

	public static void main(String[] args) throws ExecutionException {
		PiCallable piCallable = new PiCallable();
		ArrayList<PiCallable> piThreads = new ArrayList<PiCallable>();
		Integer threadNum = 10;
		for (Integer i=0; i<threadNum; i++) {
			piThreads.add(new PiCallable());
		}
		ExecutorService executorService = Executors.newFixedThreadPool(1);
		try {
			ArrayList<Future<Double>> futures = (ArrayList<Future<Double>>) executorService.invokeAll(piThreads);
			Double sum = .0;
			for (Integer i=0; i<threadNum; i++) {
				sum += futures.get(i).get();
			}
			System.out.println("Avarage of " + threadNum + " threads: " + sum/threadNum);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		Future<Double> future = executorService.submit(piCallable);
		try {
			System.out.println("Single thread: " + future.get());
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		executorService.shutdown();

	}

}
