package edu.agh.wiet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BSTTree<T extends Comparable<T>> implements BST<T> {
	public static final Logger logger = LogManager.getLogger("BSTTree");
	
	private Node<T> root = null;
	
	@Override
	public void insert(T key) {
		if (root == null) {
			root = new Node<T>(key);
		} else {
			Node<T> current = root;
			Node<T> parent = null;
			while (current != null) {
				parent = current;
				current = (current.key.compareTo(key) > 0) ? current.left : current.right;
			}
			if (parent.key.compareTo(key) > 0)
				parent.left = new Node<T>(key);
			else
				parent.right = new Node<T>(key);
		}
	}

	@Override
	public Node<T> search(T key) {
		Node<T> current = root;
		while (current != null && current.key.compareTo(key) != 0) {
			current = (current.key.compareTo(key) < 0) ? current.left : current.right; 
		}
		return current;
	}

	@Override
	public Node<T> delete(T key) {
		//TODO: implement method
		return null;
	}
	
	@Override
	public void inOrder() {
		log("=== In order ===");
		inOrder(root);
		log("=== End in order ===");
	}

	@Override
	public void preOrder() {
		log("=== Pre order ===");
		preOrder(root);
		log("=== End pre order ===");
	}

	@Override
	public void postOrder() {
		log("=== Post order ===");
		postOrder(root);
		log("=== End Post order ===");
	}

	private void inOrder(Node<T> node) {
		if (node != null) {
			inOrder(node.left);
			log(node.key);
			inOrder(node.right);
		}
	}

	private void preOrder(Node<T> node) {
		if (node != null) {
			log(node.key);
			if (node.left != null)
				log(node.left.key);
			if (node.right != null)
				log(node.right.key);
			preOrder(node.left);
			preOrder(node.right);
		}
	}

	private void postOrder(Node<T> node) {
		if (node != null) {
			postOrder(node.left);
			postOrder(node.right);
			log(node.key);
			if (node.left != null)
				log(node.left.key);
			if (node.right != null)
				log(node.right.key);
		}
	}
	
	private void log(Object obj) {
		logger.info(obj + " ");
	}

}
