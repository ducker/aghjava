package edu.agh.wiet;

public class Main {
	public static void main(String[] args) {
		BSTTree<String> stringTree = new BSTTree<String>();
		stringTree.insert("b");
		stringTree.insert("c");
		stringTree.insert("a");
		stringTree.insert("d");
		stringTree.insert("g");
		stringTree.insert("h");
		stringTree.insert("z");
		stringTree.insert("o");
		stringTree.insert("e");
		stringTree.insert("e");
		stringTree.inOrder();
		stringTree.preOrder();
		stringTree.postOrder();
		if (stringTree.search("b") != null)
			System.out.println("Jest");
		else
			System.out.println("Nie ma");
	}

}
