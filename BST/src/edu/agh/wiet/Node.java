package edu.agh.wiet;

public class Node<T extends Comparable<T>> {
	public Comparable<T> key;
	Node<T> left, right;
	Node(Comparable<T> key) {
		this.key = key;
	}
}
