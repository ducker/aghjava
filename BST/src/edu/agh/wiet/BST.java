package edu.agh.wiet;

public interface BST<T extends Comparable<T>> {
	public void insert(T key);
	public Node<T> search(T key);
	public Node<T> delete(T key);
	public void inOrder();
	public void preOrder();
	public void postOrder();
}
