import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;


public class FilesTree {

	final static HashMap<Path, Long> fileSizes = new HashMap<Path, Long>();
	public static void main(String[] args) {
		
		System.out.println("Enter start path : ");
		 
		try (BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in))){
		    String s = bufferRead.readLine();
	 
		    Path start = Paths.get(s);
			try {
				Files.walkFileTree(start, new SimpleFileVisitor<Path>() {
					@Override
				     public FileVisitResult visitFile(Path file, BasicFileAttributes attrs)
				         throws IOException
				     {
				         //System.out.println(file);
				         fileSizes.put(file, new Long(Files.size(file)));
				         return FileVisitResult.CONTINUE;
				     }
				});
				
				Integer saveFilesLimit = 50;
				ArrayList<Map.Entry<Path, Long>> entryList = new ArrayList<Map.Entry<Path, Long>>(fileSizes.entrySet());
				Collections.sort(entryList, new MapEntryComparator());
				List<Map.Entry<Path, Long>> saveList = entryList.subList(entryList.size() - saveFilesLimit, entryList.size());
				Collections.reverse(saveList);
				try (FileOutputStream fos = new FileOutputStream("output.txt");) {
				    try (ObjectOutputStream oos = new ObjectOutputStream(fos)) {   
				    	for (Entry<Path, Long> entry : saveList) {
				    		String str = "path: " + entry.getKey().toString() + "\tsize: " + entry.getValue();
				    		System.out.println(str);
				    		oos.writeUTF(str + "\n");
				    	}
				    }
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}

	}

}
