import java.nio.file.Path;
import java.util.Comparator;
import java.util.Map;
import java.util.Map.Entry;


public class MapEntryComparator implements Comparator<Map.Entry<Path, Long>> {

	@Override
	public int compare(Entry<Path, Long> entry0, Entry<Path, Long> entry1) {
		return entry0.getValue().compareTo(entry1.getValue());
	}
	 

}