
public class SimpleThread extends Thread {
	@Override
	public void run() {
		System.out.println(this.getName());
		try {
			Thread.sleep(5 * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			System.out.println(this.getName() + " finished");
		}
		for (Integer i=0; i<1000000; i++);
	}
}
