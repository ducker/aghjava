import java.util.concurrent.Callable;


public class TestCallable implements Callable<Integer> {
	@Override
	public Integer call() throws Exception {
		System.out.println(Thread.currentThread().getName());
		try {
			Thread.sleep(5 * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			System.out.println(Thread.currentThread() + " finished");
		}
		Integer i = 0;
		for (i=0; i<1000000; i++);
		return i;
	}
}
